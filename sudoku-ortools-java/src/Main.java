/**
 * @author Michael Valdron
 */

import java.util.Arrays;

public class Main {
    static { 
        System.loadLibrary("jniortools");
    } 
    /**
     * @param args
     */
    public static void main(String[] args) throws Exception {
        Puzzle p;
        PuzzleGenerator g;
        if (args.length == 0 || args[0] == null || args[0].equals("")) {
            System.err.println("ERROR: You need to specify the input filepath or the max constants to be generated.");
            return;
        } else if (args.length == 1 || args[1] == null || args[1].equals("")) {
            System.err.println("ERROR: You need to specify the puzzle dimensions.");
            return;
        }

        p = new Puzzle(Integer.parseInt(args[1]));

        try {
            Integer.parseInt(args[0]);
            g = new PuzzleGenerator(p.getDim(), Integer.parseInt(args[0]));
            p.load(g.generate());
        } catch (NumberFormatException e) {
            p.loadFile(args[0]);
        }

        System.out.println("\nInit:");
        System.out.println(p.toString()
                .replaceAll(", \\[", "\n\\[")
                .replaceAll("\\[\\[", "\\[")
                .replaceAll("\\]\\]", "\\]"));
        p.makeConstraints();
        System.out.println("\nPuzzle Solution:");
        System.out.println(Arrays.deepToString(p.solve())
                .replaceAll(", \\[", "\n\\[")
                .replaceAll("\\[\\[", "\\[")
                .replaceAll("\\]\\]", "\\]"));
    }
}
