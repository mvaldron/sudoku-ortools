/**
 * @author Michael Valdron
 * @see https://github.com/kenpu/summer-research-2018/blob/master/sandbox/2018-05-29:sudoku-ortools/solver.py
 */

import com.google.ortools.constraintsolver.DecisionBuilder;
import com.google.ortools.constraintsolver.IntVar;
import com.google.ortools.constraintsolver.Solver;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Set;

public class Puzzle {
    public static final int DIM_MIN = 4;
    public static final String BLANK_SPACE_TOKEN = "_";

    private int dim;
    private int blockSize;
    private IntVar[][] entries;
    private Solver s;
    private Hashtable<String, int[]> coord;

    public Puzzle(int d) throws Exception {
        dim = d;
        blockSize = (int)Math.sqrt(d);
        if (dim < DIM_MIN || (Math.sqrt(d) % blockSize) != 0)
            throw new Exception("ERROR: Invalid Dimensions.");
        entries = new IntVar[dim][dim];
        s = new Solver("Sudoku");
        coord = new Hashtable<String, int[]>();
    }

    public Puzzle() throws Exception {
        this(DIM_MIN);
    }

    private IntVar parseEntry(int i, int j, String x) {
        String label = String.format("var%d_%d", i, j);
        coord.put(label, new int[]{i, j});
        if (x.equals(BLANK_SPACE_TOKEN)) {
            return s.makeIntVar(1, dim, label);
        } else {
            return s.makeIntConst(Integer.parseInt(x), label);
        }
    }

    private IntVar[] getFlatArray() {
        IntVar[] flatEntries = new IntVar[dim*dim];
        Set<String> keys = coord.keySet();

        // Flatten Matrix
        for (String k : keys) {
            flatEntries[coord.get(k)[0]*dim + coord.get(k)[1]] = entries[coord.get(k)[0]][coord.get(k)[1]];
        }
        return flatEntries;
    }

    public void loadEmpty() {
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                entries[i][j] = parseEntry(i,j,BLANK_SPACE_TOKEN);
            }
        }
    }

    public void loadFile(String p) {
        FileReader rdr;
        BufferedReader buffr;
        String line;
        String[] aline;
        int i = 0;
        int j;

        try {
            rdr = new FileReader(p);
            buffr = new BufferedReader(rdr);
            while ((line = buffr.readLine()) != null) {
                j = 0;
                aline = line.trim().split(" ");
                for (String x: aline) {
                    entries[i][j] = parseEntry(i,j,x);
                    j++;
                }
                i++;
            }
            buffr.close();
            rdr.close();
        } catch (IOException e) {
            System.err.println("ERROR: " + e.getMessage());
        }
    }

    public void load(String[] m) {
        int i = 0;
        int j;
        for (String line: m) {
            j = 0;
            for (String x: line.trim().split(" ")) {
                entries[i][j] = parseEntry(i,j,x);
                j++;
            }
            i++;
        }
    }

    public void load(String[][] m) {
        int i = 0;
        int j;
        for (String[] line: m) {
            j = 0;
            for (String x: line) {
                entries[i][j] = parseEntry(i,j,x);
                j++;
            }
            i++;
        }
    }

    public void load(int[][] m) {
        System.arraycopy(m, 0, entries, 0, dim*dim);
    }

    public void load(IntVar[][] m) {
        entries = m;
    }

    public void makeConstraints() {
        // Define Constraints

        // Rows
        for (int i = 0; i < dim; i++) {
            IntVar[] row = new IntVar[dim];
            for (int j = 0; j < dim; j++) {
                row[j] = entries[i][j];
            }
            s.addConstraint(s.makeAllDifferent(row));
        }

        // Columns
        for (int j = 0; j < dim; j++) {
            IntVar[] col = new IntVar[dim];
            for (int i = 0; i < dim; i++) {
                col[i] = entries[i][j];
            }
            s.addConstraint(s.makeAllDifferent(col));
        }

        // Boxes
        for (int i = 0; i < blockSize; i++) {
            for (int j = 0; j < blockSize; j++) {
                IntVar[] box = new IntVar[dim];
                for (int di = 0; di < blockSize; di++) {
                    for (int dj = 0; dj < blockSize; dj++) {
                        box[di*blockSize+dj] = entries[i*blockSize+di][j*blockSize+dj];
                    }
                }
                s.addConstraint(s.makeAllDifferent(box));
            }
        }
    }

    public int[][] solve() {
        int[][] sol = new int[dim][dim];
        IntVar[] flatEntries = getFlatArray();
        DecisionBuilder db = s.makePhase(flatEntries, Solver.CHOOSE_FIRST_UNBOUND, Solver.ASSIGN_MIN_VALUE);
        Set<String> keys = coord.keySet();

        s.newSearch(db);

        if (s.nextSolution()) {
            for (String k : keys) {
                sol[coord.get(k)[0]][coord.get(k)[1]] = (int) entries[coord.get(k)[0]][coord.get(k)[1]].value();
            }
        } else {
            sol = null;
        }

        s.endSearch();

        return sol;
    }

    @Override
    public String toString() {
        return Arrays.deepToString(entries);
    }

    public int getDim() {
        return dim;
    }

    public String[] getVarNames() {
        return (String[]) coord.keySet().toArray();
    }
}
