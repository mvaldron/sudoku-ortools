/**
 * @author Michael Valdron
 */

import java.util.Arrays;
import java.util.Random;

public class PuzzleGenerator {
    private int dim;
    private int maxConstants;

    public PuzzleGenerator(int dim, int maxConstants) {
        this.dim = dim;
        this.maxConstants = maxConstants;
    }

    public PuzzleGenerator(int maxConstants) {
        this(Puzzle.DIM_MIN, maxConstants);
    }

    private int[] aRange(int a, int b) {
        int[] arr = new int[(b-a)+1];
        int k = a;
        for (int i = 0; i < arr.length; i++) {
            arr[i] = k;
            k++;
        }
        return arr;
    }

    private int getRandom(int[] arr) {
        Random rand = new Random();
        rand.setSeed(rand.nextInt());
        int n = rand.nextInt(arr.length);
        return arr[n];
    }

    public String[][] generate() throws Exception {
        String[][] grid = new String[dim][dim];
        Puzzle p;
        int[] choices = aRange(1, dim);
        int[] idx = aRange(0, (dim - 1));
        int k = 0;
        int i;
        int j;
        String x;

        for (int l = 0; l < dim; l++) {
            Arrays.fill(grid[l], Puzzle.BLANK_SPACE_TOKEN);
        }

        while (k < maxConstants) {
            i = getRandom(idx);
            j = getRandom(idx);
            if (grid[i][j].equals(Puzzle.BLANK_SPACE_TOKEN)) {
                p = new Puzzle(dim);
                x = Integer.toString(getRandom(choices));
                grid[i][j] = x;
                p.load(grid);
                p.makeConstraints();
                if (p.solve() != null) {
                    k++;
                } else {
                    grid[i][j] = Puzzle.BLANK_SPACE_TOKEN;
                }
            }
        }

        return grid;
    }

}
