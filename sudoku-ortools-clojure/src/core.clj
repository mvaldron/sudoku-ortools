(ns sudoku-ortools-clojure.core)
;;   (:require [com.google.ortools.constraintsolver.Solver]))

(defn load-libs [libpath]
    (System/load (str libpath "/libcvrptw_lib.so"))
    (System/loadLibrary "cvrptw_lib")
    (System/load (str libpath "/libdimacs.so"))
    (System/loadLibrary "dimacs")
    (System/load (str libpath "/libfap.so"))
    (System/loadLibrary "fap")
    (System/load (str libpath "/libortools.so"))
    (System/loadLibrary "ortools")
    (System/load (str libpath "/libjniortools.so"))
    (System/loadLibrary "jniortools"))

(defn main []
    (load-libs (str (System/getProperty "user.dir") "/lib")))
;;     (let [s (new Solver nil)
;;           x (.makeIntVar (s) 1 4 "x")]
;;         ));;(println x)))

(main)
