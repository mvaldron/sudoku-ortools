#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 24 17:44:51 2018

@author: Michael Valdron
"""

import argparse as argp
import numpy as np
import random as rand
from ortools.constraint_solver import pywrapcp

def parse_int(i):
    try:
        int(i)
        return True
    except ValueError:
        return False

def S(I, prints=True):
    solver = pywrapcp.Solver('sudoku-solver')
    # Variables and Constants
    M = np.asarray([np.asarray([solver.IntConst(int(I[i, j]), "M%d%d" % (i, j))\
        if I[i, j] != None else solver.IntVar(1, 4, "M%d%d" % (i, j))\
        for j in range(len(I[i]))])\
        for i in range(len(I))])

    # Define Constraints

    # Rows
    for i in range(4):
        solver.Add(solver.AllDifferent(list(M[i,:])))

    # Columns
    for i in range(4):
        solver.Add(solver.AllDifferent(list(M[:,i])))

    # Boxes
    for i in range(0, 2, 2):
        for j in range(0, 2, 2):
            solver.Add(solver.AllDifferent(list(M[i:i+2,j:j+2].reshape(4))))

    solver.Solve(solver.Phase(list(M.reshape(16)), solver.CHOOSE_FIRST_UNBOUND, solver.ASSIGN_MIN_VALUE))

    O = np.empty([4,4], dtype=np.int64)
    # Prints possible solutions
    count = 0
    while solver.NextSolution():
        if prints:
            print("Solution %d:" % (count+1))
        for i in range(4):
            line = ""
            for j in range(4):
                O[i,j] = np.int64(M[i,j].Value())
                line += str(O[i,j])
                if j < 3:
                    line += " "
            if prints:
                print(line)
        if prints:
            print()
        count += 1

    if prints:
        if count > 0:
            print("%d puzzle solutions found." % (count))
        else:
            print("No solutions found.")

    return O, count

def is_solv(I):
    _, count = S(I, False)
    return count > 0

def main():
    # User arguments
    parser = argp.ArgumentParser(description='Python Sudoku Solver using Google\'s Optization Tools (ortools).')
    parser.add_argument('-f', type=str, dest='filepath', help='Path to file with presetted board.')
    parser.add_argument('-o', type=str, dest='outfile', help='Path to file for outputted board preset.')
    parser.add_argument('-c', type=int, dest='c', help='Max constants to be generated.', default=8)
    args = parser.parse_args()

    I = np.empty([4,4], dtype=object)

    if args.filepath != None:
        with open(args.filepath, 'r') as f:
            i = 0
            print("Sudoku Puzzle Preset:")
            for line in f:
                print(line.strip())
                line_vec = line.strip().split(' ')
                for j in range(len(line_vec)):
                    # If set then declare value as constant else declare unknown variable
                    if parse_int(line_vec[j]):
                       I[i,j] = line_vec[j]
                i += 1
            print()
        S(I)
    else:
        max_constants = args.c
        i = 0
        while i < max_constants:
            j = [rand.choice(np.arange(0, len(I), dtype=np.int64)) for _ in range(2)]
            if I[j[0], j[1]] == None:
                I[j[0], j[1]] = str(rand.choice(np.arange(1, 5, dtype=np.int64)))
                if is_solv(I):
                    i += 1
                else:
                    I[j[0], j[1]] = None
        if args.outfile != None:
            with open(args.outfile, 'w') as f:
                for i in range(len(I)):
                    line = ""
                    for j in range(len(I[i])):
                        if I[i,j] == None:
                            line += "_"
                        else:
                            line += I[i,j]
                        if j < (len(I[i]) - 1):
                            line += " "
                    f.write(line + '\n')
        print("\nGenerated Sudoku Puzzle:")
        for i in range(len(I)):
            line = ""
            for j in range(len(I[i])):
                if I[i,j] == None:
                    line += "_"
                else:
                    line += I[i,j]
                if j < (len(I[i]) - 1):
                    line += " "
            print(line)

if __name__ == "__main__":
    main()
